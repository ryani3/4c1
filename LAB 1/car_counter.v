`timescale 1ns / 1ps



module car_counter(
    input wire clk,reset,                                                                                
    input enter, exit,  
    input sequence_detected, 
    output reg [15:0] counter

    );
    
    
    always @(posedge clk or posedge reset)                                          
    begin                                                                           
        if (reset) begin                                                            
            counter <= 0; //set counter to 0 i.e no cars                                                                        
        end else begin                                                              
            if (enter == 1)                                                           
                counter <= counter + 1;   // if enter is 1 i.e. enter sequence detected increment counter by 1                                        
            else if (exit == 1)                                                                    
                counter <= counter - 1; //else if exit is 1 i.e. exit sequence detected decrement counter by 1                                            
            end                                                                     
       end  


                                                                           
endmodule
