`timescale 1ns / 1ps

module counter_testbench();

    //initialise values
    reg  clk, reset;
    //reg a, b;
    reg enter, exit;
    wire [15:0] counter;
    
    //call the modules
    //FSM_sensor uut (.clk(clk), .reset(reset), .a(a), .b(b), .enter(enter), .exit(exit));
    car_counter uut (.clk(clk), .reset(reset), .enter(enter), .exit(exit), .counter(counter));

    localparam T = 2;
    
    initial begin 
        clk = 1'b1; //set clock 
            forever #(T/2) clk = !clk;
    end
    
    initial begin
        reset = 1'b1; //set reset
        #T;
        reset = 1'b0;
    end


    initial begin 
    
    enter = 1;
    #(T);
    enter = 1;
    #(T);
    enter = 1;
    #(T);
    enter = 1;
    #(T);
    exit = 1;
    #(T);
    exit = 1;
    #(T);
    
    end



endmodule