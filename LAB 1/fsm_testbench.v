`timescale 1ns / 1ps

module FSM_testbench();
  
    //initialise values
    reg  clk, reset;
    reg a, b;
    wire enter, exit;
    //wire counter;
    
    //call the modules
    FSM_sensor uut (.clk(clk), .reset(reset), .a(a), .b(b), .enter(enter), .exit(exit));
   //car_counter cnt (.clk(clk), .reset(reset), .enter(enter), .exit(exit), .counter(counter));
       
    localparam T = 2;
    
    initial begin 
        clk = 1'b1; //set clock 
            forever #(T/2) clk = !clk;
    end
    
    initial begin
        reset = 1'b1; //set reset
        #T;
        reset = 1'b0;
    end
    

    //test sequence detection
    initial begin 
        //send in enter sequence 00 10 11 01 00
        a = 0; b = 0; // 00
        #(T);
        a = 1; b = 0; // 10
        #(T);
        a = 1; b = 1; // 11
        #(T);
        a = 0; b = 1; // 01
        #(T);
        a = 0; b = 0; // 00
        #(T);

        // send in exit sequence 00 01 11 10 00
        a = 0; b = 0; // 00
        #(T);
        a = 0; b = 1; // 01
        #(T);
        a = 1; b = 1; // 11
        #(T);
        a = 1; b = 0; // 10
        #(T);
        a = 0; b = 0; // 00
        #(T);
        
        //random sequence
        a = 0; b = 0; 
        #(T);
        a = 0; b = 0; 
        #(T);
        a = 1; b = 1; 
        #(T);
        a = 0; b = 0; 
        #(T);
        a = 1; b = 1; 
        #(T);

    end


 
endmodule

