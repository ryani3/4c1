`timescale 1ns / 1ps


module FSM(
    input clk, reset, 
    input a, b, 
    output reg enter, exit
    );
    
   //initialise states
   localparam [2:0] S0 = 3'b000,
                    S1 = 3'b010,
                    S2 = 3'b001,
                    S3 = 3'b011,
                    S4 = 3'b100;

                    
               
               
  reg [2:0] state_reg, next_state;
  
      always @(negedge clk) begin
        if (reset)begin
            state_reg <= S0; //set back to state 0 when reset is asserted
            //enter <= 0;
            //exit <= 0;
         // the rest of the time move onto next module fsm
            end
            else begin
            state_reg <= next_state;
            end
            end
            

            
        always @(*)
        begin
        enter = 0; // initialise enter and exit variables to 0 i.e. low
        exit = 0;
        next_state = state_reg;
            case (state_reg)
                S0: begin //sensors unblocked i.e. a = 0, b = 0, default
                // if either enter or exit sequence go to next state
                if(a == 1 && b == 0) begin // if a = 1, b = 0 : enter
                next_state = S1;
                //enter = 1'b0;
                //exit = 1'b0;
                end
                else if (a == 0 && b == 1) begin
                next_state = S1; //go to next state
                end
                end
                
             S1: begin
                if(a == 1 && b == 1) // if input is 11 (same for both enter and exit sequence)
                next_state = S2; // go to state 2
                end

             S2: begin
                if(a == 0 && b == 1) // if input is 01 i.e. sensor b is covered and a is not
                //enter = 1'b1; //car has entered, set enter to high
                next_state = S3;
                
                else if(a == 1 && b == 0)//if input is 10, a is covered and b is not
                //exit = 1'b1; //car has exited, set exit to high
                next_state = S4;
                
                end
                
             S3: begin 
                 enter = 1;
                 next_state = S0;
                 end 
                 
             S4: begin 
                 exit = 1;
                 next_state = S0;
                 end    
 
                default: next_state = S0; // go back to default state 00
                
                endcase
                
                end

                
                
                
endmodule
