`timescale 1ns / 1ps



module car_counter(
    input wire clk, reset,                                                                                
    input enter, exit,
    output reg [3:0] count

    );
    
    initial begin
        count = 0;
    end
    
            
    always @(posedge clk)                                          
    begin                                                                           
        if (reset) begin                                                            
            count <= 0; //set counter to 0 i.e no cars                                                                        
        end else begin                                                              
            if (enter == 1 && count < 15)  // if enter = 1 and less than 15 cars in car park                                                        
                count <= count + 1;   // if enter is 1 i.e. enter sequence detected increment counter by 1                                        
            else if (exit == 1 && count > 0) // if exit = 1 and theres more than 0 cars                                                                 
                count <= count - 1; //else if exit is 1 i.e. exit sequence detected decrement counter by 1                                            
            end 
            end                                                              
     

                                                                           
endmodule