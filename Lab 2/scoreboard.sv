`timescale 1ns / 1ps

module scoreboard(
        input clk, reset,
        input inc_exp, dec_exp,
        input reg enter, exit,
        input [3:0] count, [3:0] exp_count
         

    );
    
    //takes in expected increment and decrement from stim_gen
    //actual inc and dec come from FSM
    
    reg [64:0] err_msg;
    //print to file
    integer file;
    
    initial begin 
        //print headers for easier reading
        //tcl console
        $display("Time  Reset  Enter  Exit   Expected Inc  Expected Dec  Count   Expected Count   Error Message");
        //file
        //open file 
        file = $fopen("C:/Users/RYANI3/Lab2/log.txt", "w");
        if(!file) //if file not found
            begin //dispaly on tcl consile
            $display("File not found");
        end
        //print to log file
        $fdisplay(file, "Time  Reset  Enter  Exit   Expected Inc  Expected Dec  Count   Expected Count   Error Message");
        
        
        end

    always @(posedge clk) begin

    //compare the expected increment with the actual increment
    if (inc_exp == enter && dec_exp == exit && exp_count == count) 
        err_msg = "correct"; // print nothing if it passes
        else 
         err_msg = "error"; //print error if it doesnt
        
        $display("%4d,    %b     %b      %b         %b              %b        %d       %d           %s",
                    $time, reset, enter, exit, inc_exp, dec_exp, count, exp_count, err_msg);
        
        $fdisplay(file, "%4d,    %b     %b      %b         %b              %b        %d       %d           %s",
                    $time, reset, enter, exit, inc_exp, dec_exp, count, exp_count, err_msg);
                    
        end
        
        
        
    
endmodule

