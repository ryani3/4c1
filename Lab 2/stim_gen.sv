`timescale 1ns / 1ps



module stim_gen
    #(parameter T = 20)
    (
        output reg clk, reset, a, b,
        output reg inc_exp, dec_exp,
        output reg [3:0] exp_count
       
    

    );
    
    
    
    //clock
    always
    begin
        clk = 1'b1;
        #(T/2);
        clk = 1'b0;
        #(T/2);
    end
    
    //test procedure
    //call all tasks 
    initial
    begin
        initialise();
        enter_task(4); //enter 4 times
        exit_task(4); //exit 4 times 
        enter_task(5); //enter 5 times
        exit_task(7); //exit 7 times 
        // more cars are being exited than currently in the carpark
       
       
        $stop; 
    end
    
    
        //#40 //wait 40 to differentiate easier
        //load_data(8'b0100_0000, 8'b1110_0000); //check if count works by inputing the enter 
        //#40 //wait 40
        //load_data(8'b1110_0000, 8'b0100_0000); //check exit

         #5 //wait 5 to differentiate easier
        load_data(8'b0100_0000, 8'b1110_0000); //check if count works by inputing the enter 
        #5 //wait 5
        load_data(8'b1110_0000, 8'b0100_0000); //check exit
        #1000
        
        //enter_task(6); //enter 6 times
        //exit_task(1); //exit 1 times 
        //enter_task(4); //enter 4 times
        //exit_task(6); //exit 6 times 
        
    
    //define tasks
    task initialise();
    begin 
        
        a = 0;
        b = 0;
        reset = 0;
        inc_exp = 0;
        dec_exp = 0;
        exp_count = 0;
    end
    endtask 
    
    task enter_task(input integer C);
    begin
    repeat(C) @(posedge clk) begin
    //enter sequence
        a = 0;
        b = 1;
        #T
        a = 1;
        b = 1;
        #T
        a = 0;
        b = 1;
        #T
        a = 0;
        b = 0;
        
        inc_exp = 1;
        #T
        inc_exp = 0;
 
        if (exp_count < 15) 
            exp_count = exp_count + 1;
        else
            exp_count = exp_count;
        
    end    
        
        
        
    end 
    endtask
            
            
    task exit_task(input integer C);
    begin
    repeat(C) @(posedge clk) begin
    //exit sequence
        a = 1;
        b = 0;
        #T
        a = 1;
        b = 1;
        #T
        a = 1;
        b = 0;
        #T
        a = 0;
        b = 0;

        dec_exp = 1;
        #T
        dec_exp = 0;
        
        
        if (exp_count > 0) 
            exp_count = exp_count - 1;
        else
            exp_count = exp_count;
        
    end 
        
        
    end 
    endtask
    
    
    task load_data(input reg [7:0] a_data, b_data); 
    begin: ld
    
    integer i;
    
        for(i = 0; i < 8; i = i + 1) begin
        a = a_data[i];
        b = b_data[i];
        #T;
        end
   
    end
    endtask
    
    task reset_task();
    begin
        @(posedge clk);
        reset = 1'b1;
        @(posedge clk);
        reset = 1'b0;
    end
    endtask
    
    
    
    
    
    
endmodule
