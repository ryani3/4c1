`timescale 1ns / 1ps



module top_module();

    
    wire clk, reset, a, b, inc_exp, dec_exp, enter, exit;
    wire [3:0] count;
    wire [3:0] exp_count;
    

   
    
    stim_gen stim (.clk(clk), .reset(reset), .a(a), .b(b), .inc_exp(inc_exp), .dec_exp(dec_exp), .exp_count(exp_count));
    
    FSM fsm (.clk(clk), .reset(reset), .a(a), .b(b), .enter(enter), .exit(exit));
    
    car_counter cnt (.clk(clk), .reset(reset), .enter(enter), .exit(exit), .count(count));
    
    scoreboard monitor (.clk(clk), .reset(reset), .inc_exp(inc_exp), .dec_exp(dec_exp), .enter(enter), .exit(exit), .count(count), .exp_count(exp_count));
    
    
    endmodule
    